# Polestar Theme

A light and dark theme in line with Polestar's brand.

![Light theme preview](https://gitlab.com/datahjelpen/public/extensions/polestar-theme/-/raw/main/preview-light.jpg)

![Dark theme preview](https://gitlab.com/datahjelpen/public/extensions/polestar-theme/-/raw/main/preview-dark.jpg)

## How release a new version

### Light

1. npm run package:light
2. npm run publish:light

### Dark

1. npm run package:dark
2. npm run publish:dark
