# Polestar Theme

A theme in line with Polestar's brand.

![Theme preview](https://gitlab.com/datahjelpen/public/extensions/polestar-theme/-/raw/main/preview-light.jpg)

Made by Datahjelpen AS
