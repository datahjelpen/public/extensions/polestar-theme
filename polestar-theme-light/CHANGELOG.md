# Change Log

All notable changes to the "polestar-theme" extension will be documented in this file.

## [0.1.3] - 2022-04-27

- Rename to "Polestar Light Theme"

## [0.1.2] - 2022-04-27

- Rename to "Polestar Light Theme"

## [0.1.1] - 2022-04-26

- Update syntax highlighting

## [0.1.0] - 2022-04-21

- Finalize colors

## [0.0.3] - 2022-04-21

- Adjust banner
- Add license

## [0.0.3] - 2022-04-21

- Adjust banner
- Adjust colors

## [0.0.2] - 2022-04-21

- Add icon
- Add banner
- Adjust colors

## [0.0.1] - 2022-04-21

- Initial release
