# Polestar Dark Theme

A theme in line with Polestar's brand.

![Theme preview](https://gitlab.com/datahjelpen/public/extensions/polestar-theme/-/raw/main/preview-dark.jpg)

Made by Datahjelpen AS
