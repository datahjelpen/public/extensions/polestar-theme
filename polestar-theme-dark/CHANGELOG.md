# Change Log

All notable changes to the "polestar-theme-dark" extension will be documented in this file.

## [0.0.3] - 2022-04-27

- Update banner
- Update logo

## [0.0.2] - 2022-04-27

- Rename to "Polestar Light Theme"

## [0.0.1] - 2022-04-27

- Initial release
